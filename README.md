# pcb-MEMS_HV

Design files for a [Sinara Zotino](https://github.com/sinara-hw/Zotino/wiki) compatible HV Amplifier. 

# Libraries
This project uses the [DQC KiCAD Libraries](https://gitlab.com/duke-artiq/pcb/dqc-kicad-libraries)